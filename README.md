lustre_client
=============

Installs the Lustre client (packages come from a website or local NFS server) and mounts the storage.

- Check lustre version, and if needed:
  - remove Lustre
  - copy packages (from a website or NFS local server)
  - install client lustre
- configure lustre kernel module  
  Mainly to specify the network interface to use
- mount Lustre moutpoints


Role Variables
--------------

Variables are explained in `default/main.yml' vars file.

`lustre_client_version`: lustre version to install

`lustre_client_pkg_dir`: directory where packages are store

Get packages from:  
`lustre_repo_base_url`: URL (default from whamcloud).  
`lustre_client_nfs_server`, `lustre_client_nfs_dir`: NFS local repository (priority over URL)  

`lustre_client_network`: force the network interface to use (usefull if several networks). Just define the network (example: 192.168.16.0).

`lustre_mounts`: Mount points. List of ['src, 'path', 'opts']


Requirements
------------
No requirements


Dependencies
------------
No Dependencies


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - lustre_client

License
-------

MIT

Author Information
------------------

Institut Français de Bioinformatique

https://www.france-bioinformatique.fr/
